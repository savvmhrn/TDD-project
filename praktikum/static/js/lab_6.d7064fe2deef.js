$ (document).ready(function(){ //ketika server dibuka, dia jalan
  var part = "msg-receive" //tombol chat
  $("#Arrow").click(function(){
      $(".chat-body").toggle();
      var src=($(this).attr('src')==='https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png')?
      'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_up-16.png' : //kalo up jadi down kalo down jadi up
      'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png';

    $(this).attr('src',src);
  });
});

var object = document.getElementsByClassName("enter-text");
var output = document.getElementsByClassName("out");
var newest = document.getElementsByClassName('enterya')

newest.onclick = fuction(){
  output.innerHTML +=object.value + "<br>" //isi ditaro dalam html output , yang ditulis jadi kosong
  object.value="";

  return true;
}

object.onkeyup = function(e){
  e = e || event;
  if (e.keyCode === 13 && !e.shiftKey) { //kalo enter&shiftkey jadi return kebawah
    out.innerHTML+=obj.value+"<br>";
    object.value="";

  }
  return true;
 }

//END Chatbox

var print = document.getElementById('print'); //print layar kalkulator
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value="";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
	} else if (x === 'log' || x ==='sin' || x === 'tan') {
		switch (x) {
			case 'log':
				print.value = Math.log10(print.value);
				erase = true;
				break;
			case 'sin':
				print.value = Math.sin(print.value*(Math.PI/180));
				erase = true;
				break;
			case 'tan':
				print.value = Math.tan(print.value*(Math.PI/180));
				erase = true;
				break;
	}

  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

var themes = [
               {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#212121"},
               {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#212121"},
               {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#212121"},
               {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#212121"},
               {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
               {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
               {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
               {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
               {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
               {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
               {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#212121"}
               ];

//inisiasi data ke JSON
localStorage.setItem('themes', JSON.stringify(themes));

// Retrieve the object from storage
var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');


$(document).ready(function() {
    $('.my-select').select2({
        'data' : JSON.parse(retrievedObject)
    });

    var retrievedTheme = localStorage.getItem('newSelected');
    var appliedTheme = JSON.parse(retrievedTheme);
    $('body').css('background', appliedTheme.selected.bcgColor);
    $('body').css('color', appliedTheme.selected.fontColor);
});

$('.apply-button').on('click', function(){  // sesuaikan class button
// [TODO] ambil value dari elemen select .my-select
    var id = $(".my-select").select2("val");

    data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
    if(id == item.id){
        var text = item.text;
        var background = item.bcgColor;
        var font = item.fontColor;

        $('body').css("background-color",background);
        $('body').css("color", font);

        var tes = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
        localStorage.setItem('newSelected', JSON.stringify(tes));
        }
    });
});
